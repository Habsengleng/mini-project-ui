import { Component, Input, OnChanges, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { BookService } from '../services/book.service';

const DEFAULT_IMG = '../../assets/imgs/default-thumbnail.jpg';
@Component({
  selector: 'app-book-table',
  templateUrl: './book-table.component.html',
  styleUrls: ['./book-table.component.css'],
})
export class BookTableComponent implements OnInit, OnChanges {
  page = 1;
  @Input() title: string;
  books: any[];
  constructor(private _book: BookService, private router: Router) {}
  ngOnChanges() {
    this.onSearch();
  }
  ngOnInit(): void {
    this.getBooks();
  }
  getBooks(): void {
    this._book
      .getBooks()
      .subscribe((arg) => (this.books = arg._embedded.books));
  }
  onSearch() {
    if (this.title == '') {
      this.ngOnInit();
    } else {
      this.books = this.books.filter((res) => {
        return res.title
          .toLocaleLowerCase()
          .match(this.title.toLocaleLowerCase());
      });
    }
  }
  deleteBook(id: number): void {
    this._book
      .delete(id)
      .subscribe(
        () => (this.books = this.books.filter((item) => item.id !== id))
      );
  }
}
