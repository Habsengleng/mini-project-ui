import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BookFormComponent } from './book-form/book-form.component';
import { CategoryFormComponent } from './category-form/category-form.component';
import { HomeComponent } from './home/home.component';
import { ViewBookComponent } from './view-book/view-book.component';

const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'addBook', component: BookFormComponent },
  { path: 'addCategory', component: CategoryFormComponent},
  {path: 'updateBook', component: BookFormComponent},
  {path: 'viewBook/:id', component: ViewBookComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
