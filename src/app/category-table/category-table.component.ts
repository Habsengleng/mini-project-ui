import {
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output,
} from '@angular/core';

import { CategoryService } from '../services/category.service';

@Component({
  selector: 'app-category-table',
  templateUrl: './category-table.component.html',
  styleUrls: ['./category-table.component.css'],
})
export class CategoryTableComponent implements OnInit, OnChanges {
  categories: any[];
  id: number;
  constructor(private _cateApi: CategoryService) {}
  @Input() category: any;
  @Output() childButtonEvent = new EventEmitter();
  ngOnInit(): void {
    this.getData();
  }
  ngOnChanges() {
    this.getData();
  }
  getData(): void {
    this._cateApi.get().subscribe((res) => {
      this.categories = res._embedded.categories;
    });
  }
  deleteCategory(id: number): void {
    this._cateApi.delete(id).subscribe((res) => {
      this.getData();
    });
  }
  getCategoryId(id: number): void {
    this.childButtonEvent.emit(id);
    this.id = id;
  }
}
