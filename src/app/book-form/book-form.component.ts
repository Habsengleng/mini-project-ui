import { HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { BookService } from '../services/book.service';
import { CategoryService } from '../services/category.service';

const DEFAULT_IMG = '../../assets/imgs/default-thumbnail.jpg';
@Component({
  selector: 'app-book-form',
  templateUrl: './book-form.component.html',
  styleUrls: ['./book-form.component.css'],
})
export class BookFormComponent implements OnInit {
  defaultImg = DEFAULT_IMG;
  bookForm: FormGroup;
  submitted = false;
  category: any[];
  imgUrl: string;
  file: '';
  bookId: any;
  isChange = false;
  books: any;
  isUpdate = false;
  constructor(
    private fb: FormBuilder,
    private _cate: CategoryService,
    private _book: BookService,
    private _activateRoute: ActivatedRoute,
    private router: Router,
  ) {}

  ngOnInit(): void {
    this.createForm();
    this.getRadioButton();
    this.getQueryParam();
  }
  createForm(): void {
    this.bookForm = this.fb.group({
      title: ['', Validators.required],
      author: ['', Validators.required],
      description: ['', Validators.required],
      category: ['', Validators.required],
    });
  }
  handleFileInput(e: any): void {
    this.file = e.target.files[0];
    this.isChange = true;
    if (this.file) {
      const reader = new FileReader();
      reader.readAsDataURL(e.target.files[0]);
      reader.onload = (event: any) => {
        this.defaultImg = event.target.result;
      };
    }
  }
  get f(): any {
    return this.bookForm.controls;
  }
  onSubmit(data: any): void {
    this.submitted = true;
    if (this.bookForm.invalid) {
      return;
    } else {
      const formData = new FormData();
      formData.append('file', this.file);
      if (this.isChange === false) {
        this.books = {
          title: this.bookForm.get('title')?.value,
          author: this.bookForm.get('author')?.value,
          description: this.bookForm.get('description')?.value,
          thumbnail: this.defaultImg,
          category: {
            id: this.bookForm.get('category')?.value,
          },
        };
        if (this.isUpdate) {
          this._book.update(this.bookId, this.books).subscribe((res) => {
            this.router.navigate(['/']);
          });
        } else {
          this._book.postBook(this.books).subscribe((res) => {
            this.clearInput();
          });
        }
      } else if (this.isChange === true) {
        this._book.uploadImage('thumbnail', formData, (cb: any) => {
          this.books = {
            title: this.bookForm.get('title')?.value,
            author: this.bookForm.get('author')?.value,
            description: this.bookForm.get('description')?.value,
            thumbnail: cb,
            category: {
              id: this.bookForm.get('category')?.value,
            },
          };
          if (this.isUpdate) {
            this._book.update(this.bookId, this.books).subscribe((res) => {
              console.log(res);
              this.router.navigate(['/']);
            });
          } else {
            this._book.postBook(this.books).subscribe((res) => {
              this.clearInput();
            });
          }
        });
      }
    }
  }
  onDataChange(e: any): void {
    console.log(this.bookForm.get('category')?.value);
  }
  getRadioButton(): void {
    this._cate.get().subscribe((res) => {
      this.category = res._embedded.categories;
    });
  }
  clearInput(): void {
    this.bookForm.reset();
    this.bookForm.get('title')?.clearValidators();
    this.bookForm.get('title')?.updateValueAndValidity();
    this.bookForm.get('author')?.clearValidators();
    this.bookForm.get('author')?.updateValueAndValidity();
    this.bookForm.get('description')?.clearValidators();
    this.bookForm.get('description')?.updateValueAndValidity();
    this.bookForm.get('category')?.clearValidators();
    this.bookForm.get('category')?.updateValueAndValidity();
    this.defaultImg = DEFAULT_IMG;
  }
  getQueryParam() {
    this._activateRoute.queryParamMap.subscribe((query: any) => {
      if (query.params.update === 'true') {
        this.isUpdate = true;
        this.bookId = query.params.bookId;
        this._book.findOne(query.params.bookId).subscribe((res) => {
          this.bookForm.setValue({
            title: res.title,
            author: res.author,
            description: res.description,
            category: res.category.id,
          });
          this.defaultImg = res.thumbnail;
        });
      }
    });
  }

}
