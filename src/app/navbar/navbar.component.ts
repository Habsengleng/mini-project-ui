import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  constructor(public translate: TranslateService) {
    translate.addLangs(['en', 'kh']);
    translate.setDefaultLang('en');
    const browserlang = translate.getBrowserLang();
    translate.use(browserlang.match(/en|kh/) ? browserlang : 'en');
  }

  ngOnInit(): void {
  }

}
