import { Component, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { CategoryService } from '../services/category.service';

@Component({
  selector: 'app-category-form',
  templateUrl: './category-form.component.html',
  styleUrls: ['./category-form.component.css'],
})
export class CategoryFormComponent implements OnInit {
  categoryForm: FormGroup;
  submitted = false;
  category: any;
  isUpdate = false;
  constructor(private fb: FormBuilder, private _cate: CategoryService) {}

  ngOnInit(): void {
    this.categoryForm = this.fb.group({
      title: new FormControl('', Validators.required),
    });
  }
  get f(): any {
    return this.categoryForm.controls;
  }
  onSubmit(data: any): void {
    this.submitted = true;
    if (this.categoryForm.invalid) {
      return;
    } else {
      if (!this.isUpdate) {
        this._cate.post(data).subscribe((res) => {
          this.category = data;
        });
      } else {
        this._cate.update(this.category.id, data).subscribe((res) => {
          this.category = data;
          this.isUpdate = false;
        });
      }
    }
    this.categoryForm.reset();
    this.categoryForm.get('title')?.clearValidators();
    this.categoryForm.get('title')?.updateValueAndValidity();
  }
  getCateID(data: any): any {
    this._cate.getCategoryId(data).subscribe((res) => {
      this.category = res;
      this.categoryForm.setValue({
        title: res.title,
      });
    });
    this.isUpdate = true;
  }
}
