import { Directive, ElementRef, HostListener, Input } from '@angular/core';

@Directive({
  selector: '[appImgFallback]',
})
export class ImgFallbackDirective {
  @Input() appImgFallback: string;
  constructor(private eRef: ElementRef) {}

  @HostListener('error')
  loadFallbackImage() {
    const element: HTMLImageElement = <HTMLImageElement>this.eRef.nativeElement;
    element.src =
      this.appImgFallback || '../../assets/imgs/default-thumbnail.jpg';
  }
}
