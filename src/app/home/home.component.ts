import { Component, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { CategoryService } from '../services/category.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
})
export class HomeComponent implements OnInit {
  categories: any[];
  title: '';
  selected = 0;
  filtered: any;
  constructor(private _category: CategoryService) {}

  ngOnInit(): void {
    this._category.get().subscribe((res) => {
      this.categories = res._embedded.categories;
    });
  }
  onSearch() {
    console.log(this.title);
  }
  onOptionsSelected() {
    console.log(this.selected);
    this.filtered = this.categories.filter((t) => t.id == this.selected);
    console.log(this.filtered);
  }
}
