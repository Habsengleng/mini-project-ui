import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BookService } from '../services/book.service';

@Component({
  selector: 'app-view-book',
  templateUrl: './view-book.component.html',
  styleUrls: ['./view-book.component.css'],
})
export class ViewBookComponent implements OnInit {
  bookId: any;
  defaultImg: string;
  book = {
    title: '',
    author: '',
    description: '',
    thumbnail: '',
    category: '',
  };

  constructor(
    private _activateRoute: ActivatedRoute,
    private _book: BookService
  ) {}

  ngOnInit(): void {
    this.getParamPath();
  }
  getParamPath() {
    this._activateRoute.paramMap.subscribe((params) => {
      this.bookId = params.get('id');
      this._book.findOne(this.bookId).subscribe((res) => {
        this.book = {
          title: res.title,
          author: res.author,
          description: res.description,
          thumbnail: res.thumbnail,
          category: res.category.title,
        };
      });
    });
  }
}
