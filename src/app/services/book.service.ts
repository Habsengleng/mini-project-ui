import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

const API_URL_BOOK = 'http://localhost:8080/api/v1/books';
@Injectable({
  providedIn: 'root',
})
export class BookService {
  constructor(private http: HttpClient) {}
  // public uploadImage(thumbnail: string, fileImage: FormData, cb: any) {
  //   return this.http
  //     .post(`${API_URL_BOOK}/${thumbnail}`, fileImage)
  //     .subscribe((res: any) => {
  //       cb(res.data);
  //     });
  // }
  uploadImage = (
    thumbnail: string,
    fileImage: FormData,
    cb: (arg0: any) => void
  ) => {
    this.http
      .post(`${API_URL_BOOK}/${thumbnail}`, fileImage)
      .subscribe((res: any) => {
        cb(res.data);
      });
  };
  public postBook(book: any): Observable<any> {
    return this.http.post(API_URL_BOOK, book).pipe(map((res) => res));
  }
  public getBooks(): Observable<any> {
    return this.http.get(API_URL_BOOK).pipe(map((res) => res));
  }
  findOne(id: number): Observable<any> {
    return this.http.get(`${API_URL_BOOK}/${id}`).pipe(map((res) => res));
  }
  getByCategory(title: string): Observable<any>{
    return this.http.get(`${API_URL_BOOK}/search/findTitle`).pipe(map((res) => res));
  }
  update(id: number, data: any): Observable<any> {
    return this.http.put(`${API_URL_BOOK}/${id}`, data);
  }
  public delete(id: number): Observable<any> {
    return this.http.delete(`${API_URL_BOOK}/${id}`).pipe(map((res) => res));
  }
}
