import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

const API_URL_CATEGORY = 'http://localhost:8080/api/v1/categories';
@Injectable({
  providedIn: 'root',
})
export class CategoryService {
  constructor(private http: HttpClient) {}

  public get(): Observable<any> {
    return this.http.get(API_URL_CATEGORY).pipe(map((res) => res));
  }
  public post(cate: any): Observable<any> {
    return this.http.post(API_URL_CATEGORY, cate).pipe(map((res) => res));
  }
  public delete(id: number): Observable<any> {
    return this.http.delete(`${API_URL_CATEGORY}/${id}`).pipe(map((res) => res));
  }
  update(id: number, data: any): Observable<any> {
    return this.http.put(`${API_URL_CATEGORY}/${id}`, data);
  }
  getCategoryId(id: number): Observable<any> {
    return this.http.get(`${API_URL_CATEGORY}/${id}`).pipe(map((res) => res));
  }
}
